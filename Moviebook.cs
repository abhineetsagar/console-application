﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieBook

{
    class Moviebook
    {

        private String movie_name;
        private String genere;
        private DateTime timing;
        private int price;
        
       

        //constructors defines here
        public Moviebook(String movie_name, String genere, DateTime timing, int price)
        {
            this.movie_name = movie_name;
            this.genere = genere;
            this.timing = timing;
            this.price = price;
          
        }





        // getters and setters defines here
        //-------------------------------------------------------------------------

        public String getmovie_name()
        {
            return movie_name;
        }

        public void setmovie_name(String movie_name)
        {
            this.movie_name = movie_name;
        }

        //--------------------------------------------------------------------------------------------------------

        public String getgenere()
        {
            return genere;
        }

        public void setgenere(String genere)
        {
            this.genere = genere;
        }


        //--------------------------------------------------------------------------------------------------------
        public DateTime gettiming()
        {
            return timing;
        }

        public void settiming(DateTime timing)
        {
            this.timing = timing;
        }

        //--------------------------------------------------------------------------------------------------------

        public int getprice()
        {
            return price;
        }

        public void setprice(int price)
        {
            this.price = price;
        }
    }
}
