﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieBook
{
    class Program
    {
        static void Main(string[] args)

        {
            Methods m = new Methods();
           p: Console.WriteLine("Movie Ticket Booking in C#\r");  //this p is used for interface and reference goto statements
            Console.WriteLine("------------------------\n");
            Console.WriteLine("1) Add any movie to list\n2) Update the movies\n3) Delete movie from the list\n4) Display all the added movies\n5) Exit\n--------------------------\n");
            string start;
            start = Console.ReadLine();
            if (start == "1")
            {
                Console.Clear();   // clears the console
                Console.WriteLine("------------------------\n");
                Console.WriteLine("You can add movies here: -\n");
                Console.WriteLine("------------------------\n");

                //call add movie method here....

                m.add();
                
                Console.WriteLine("Press * to back\n");
                string goback = Console.ReadLine();
                if (goback == "*")
                {
                    Console.Clear();  
                    goto p;

                }

            }


            if (start == "2")
            {
                Console.Clear();
                Console.WriteLine("------------------------\n");
                Console.WriteLine("Choose which movie you want to update here: -\n");
                Console.WriteLine("------------------------\n");

                //call update movies method here....

                m.update();

                Console.WriteLine("Press * to back\n");
                string goback = Console.ReadLine();
                if (goback == "*")
                {
                    Console.Clear();
                    goto p;

                }



            }

            if (start == "3")
            {
                Console.Clear();
                Console.WriteLine("------------------------\n");
                Console.WriteLine("Choose which movie you want to delete here: -\n");
                Console.WriteLine("------------------------\n");

                //call delete movies method here....

                m.delete();

                Console.WriteLine("Press * to back\n");
                string goback = Console.ReadLine();
                if (goback == "*")
                {
                    Console.Clear();
                    goto p;

                }
            }
            if (start == "4")
            {
                Console.Clear();
                Console.WriteLine("------------------------\n");
                Console.WriteLine("Display the movies you have in list: -\n");
                Console.WriteLine("------------------------\n");

                //call display movies method here....

                m.display();

                Console.WriteLine("Press * to back\n");
                string goback = Console.ReadLine();
                if (goback == "*")
                {
                    Console.Clear();
                    goto p;

                }
            }

            if (start == "5")
            {
                Environment.Exit(0);            }

            Console.ReadKey();
        }


        
    }
}
