﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieBook
{
    interface ioperations  //this ioperations triggers these following methods to call
    {
        void add();
        void update();
        void delete();
        void display();
    }
}
