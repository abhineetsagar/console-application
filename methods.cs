﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieBook
{
    public class Methods : ioperations
    {
        int no = 0;
        Moviebook[] size = new Moviebook[20];

        // writing the add method--------------------------
        public void add()
        {
            try
            {
                Console.WriteLine("------------------------\n");
                Console.WriteLine("What number of movie you want to add?");
                Console.WriteLine("------------------------\n");
                no = Convert.ToInt32(Console.ReadLine());
                for (int i = 0; i < no; i++)
                {
                    Console.WriteLine("Enter details of {0} movie/s", i + 1);
                    Console.WriteLine("\n Enter Movie name:");
                    String moviename = Console.ReadLine();
                    Console.WriteLine("\n Enter the Genere:");
                    String genere = Console.ReadLine();
                    Console.WriteLine("\n Enter the Timimg (in format HH:MM) :");
                    DateTime datetime = DateTime.Parse(Console.ReadLine());

                    datetime.ToString("HH:mm");
                    Console.WriteLine("\n Enter Ticket Price (in Rs.):");
                    int ticprice = Convert.ToInt32(Console.ReadLine());
                    size[i] = new Moviebook(moviename, genere, datetime, ticprice);

                }
            }
           
            catch (FormatException)
            {
                // Perform some action here, and then throw a new exception.
                //throw new Exception("\nDon't play with me, Enter it in the format specified please", e);
                Console.WriteLine("Enter Time in Correct format:- [HH:MM] or \nEnter correct Number for adding movies" );
                add();
            }
           
        }

        // writing the update method--------------------------
        public void update()
        {
            try
            {
                if (no == 0)
                {
                    Console.WriteLine("First add something into the list");
                    return;
                }
                int flag = 0;
                Console.WriteLine("enter the Movie number to update");
                int num = Convert.ToInt32(Console.ReadLine());
                num = num - 1;
                for (int i = 0; i < no; i++)
                {
                    if (i == num)
                    {
                        flag = 1;
                        Console.WriteLine("which field do you want to update?");
                        Console.WriteLine("\n 1.Movie name \n2.Genere \n3.Timings \n4.Ticket price ");
                        Console.WriteLine("Enter ur choice");
                        int c = Convert.ToInt32(Console.ReadLine());

                        if (c == 1)
                        {
                            Console.WriteLine("enter new name of movie");
                            String name = Console.ReadLine();
                            size[num].setmovie_name(name);
                            Console.WriteLine("Movie name updated sucessfully!!!");
                        }
                        else if (c == 2)
                        {
                            Console.WriteLine("enter updated genere");
                            string ngenere = Console.ReadLine();
                            size[num].setgenere(ngenere);
                            Console.WriteLine("Genere updated sucessfully!!!");

                        }
                        else if (c == 3)
                        {
                            Console.WriteLine("enter updated timing");
                            DateTime ngenere = DateTime.Parse(Console.ReadLine());
                            size[num].settiming(ngenere);
                            Console.WriteLine("Timings updated sucessfully!!!");

                        }
                        else if (c == 4)
                        {
                            Console.WriteLine("enter updated price (in Rs.)");
                            int nstud = Convert.ToInt32(Console.ReadLine());
                            size[num].setprice(nstud);
                            Console.WriteLine("Price updated successfully!!!");

                        }
                        else
                        {
                            Console.WriteLine("wrong choice can't update");
                            break;
                        }
                    }
                }
            }
            catch (FormatException)
            {
                // Perform some action here, and then throw a new exception.
                //throw new Exception("\nDon't play with me, Enter it in the format specified please", e);
                Console.WriteLine("Enter Time in Correct format");
                update();
               
            }
          

        }

        // writing the delete method--------------------------
        public void delete()
        {
            try
            {
                if (no == 0) { Console.WriteLine("Nothing to delete here! please add something first"); return; }
                Console.WriteLine("enter the Movie number to be deleted");
                int number = Convert.ToInt32(Console.ReadLine());
                number = number - 1;

                for (int j = 0; j < no; j++)
                {
                    if (j == number)
                    {
                        while (j < no)
                        {
                            size[j] = size[++j];
                        }
                        no = no - 1;
                    }

                }

            }
            catch (Exception)

            {
                Console.WriteLine("Please enter correct number for deletion");
                delete();

            }
           
        }

            // writing the display method--------------------------
            public void display()
            {
                if (no == 0) { Console.WriteLine("Nothing to display here! please add something first"); return; }
                for (int i = 0; i < no; i++)
                {
                    Console.WriteLine("\n DETAILS OF {0} Movie/s", i + 1);
                    Console.WriteLine("movie name :{0}", size[i].getmovie_name());

                    Console.WriteLine("Genere :{0}", size[i].getgenere());



                    Console.WriteLine("Timings :{0}", size[i].gettiming());


                    Console.WriteLine("Price:{0}", size[i].getprice());

                }
            }
        }
    }
